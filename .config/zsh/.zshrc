#!/usr/bin/env bash

source /$HOME/.profile

# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

setopt autocd autopushd
# Case-insensitive globbing (used in pathname expansion)
setopt nocaseglob
# Autocorrect typos in path names when using `cd`
# vim keybindings
bindkey -v
neofetch
