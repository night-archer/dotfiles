" Nvim config file
syntax on                       "syntax highlighting, see :help syntax
filetype plugin indent on       "file type detection, see :help filetype

set number                      "display line number
set clipboard=unnamedplus
" set cursorline
set hidden
set completeopt
set mouse=a
set splitbelow splitright
set title
set autochdir
set shell=zsh
" set shellcmdflag=-ic

" for programming
"set relativenumber              "display relative line numbers
set path+=**                    "improves searching, see :help path
set noswapfile                  "disable use of swap files
set wildmenu                    "completion menu
set backspace=indent,eol,start  "ensure proper backspace functionality
set undodir=~/.cache/nvim/undo  "undo ability will persist after exiting file
set undofile                    "see :help undodir and :help undofile
set incsearch                   "see results while search is being typed, see :help incsearch
set smartindent                 "auto indent on new lines, see :help smartindent
set ic                          "ignore case when searching
set expandtab                   "expanding tab to spaces
set tabstop=4                   "setting tab to 4 columns
set shiftwidth=4                "setting tab to 4 columns
set softtabstop=4               "setting tab to 4 columns
set showmatch                   "display matching bracket or parenthesis
set hlsearch incsearch          "highlight all pervious search pattern with incsearch

set colorcolumn=80              "display color when line reaches pep8 standards
" highlight ColorColumn ctermbg=9 "display ugly bright red bar at color column number


"----------------------------------------AutoCommands---------------------------------------- 
"autocmd VimEnter *
"   \ Vexplore |
"   \ execute "wincmd l" |
"   \ rightbelow term bash


" When python filetype is detected, F5 can be used to execute script 
autocmd FileType python nnoremap <buffer> <F5> :w<cr>:exec '!clear'<cr>:exec '!python3' shellescape(expand('%:p'), 1)<cr>


"----------------------------------------Maps---------------------------------------- 
" Keybind Ctrl+l to clear search
nnoremap <C-l> :nohl<CR><C-l>:echo "Search Cleared"<CR>


"----------------------------------------PLUGINS---------------------------------------- 
"vim-plug configuration, plugins will be installed in ~/.local/share/nvim/plugged
call plug#begin('$XDG_DATA_HOME/nvim/plugged')
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'
Plug 'nvim-lua/completion-nvim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}  
Plug 'neoclide/coc-python', {'do': 'yarn install --frozen-lockfile'}  
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

call plug#end()

runtime coc-init.vim
"--------------------Neovim LSP Configuration--------------------
lua require('lua_config')
lua require('lspconfig').pylsp.setup{}

" Enable Tab / Shift Tab to cycle completion options
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
let g:completion_enable_fuzzy_match=1
set completeopt=menuone,noinsert,noselect
nmap <Return> o<Esc>


