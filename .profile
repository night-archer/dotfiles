# PROFILE

# Environment Variables
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export PATH=$PATH:$HOME/bin:/opt/cuda/bin
export EDITOR="nvim"

# config paths
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export PYENV_ROOT="$XDG_DATA_HOME"/pyenv
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export WINEPREFIX="$XDG_DATA_HOME"/wine
export ZDOTDIR="$HOME"/.config/zsh
export QISKIT_SETTINGS="$HOME/code/quantum/qiskit.conf"

alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# specific vars
export CHROME_PATH=$(which brave)

# general
alias rm="rm -i"
alias ls="exa"
alias ll="exa -lah"
alias cgrep="grep --color=always"
alias lsf="exa -hal | cgrep -v '^d'"
alias lsdir="exa -hal | cgrep '^d'"
alias cp="cp -i" # confirm before overwriting something
alias open="xdg-open"
alias cat="bat -pp"
alias ps="procs"

alias pacinstall="sudo pacman -Syu && sudo pacman -S"
alias pacupdate="sudo pacman -Syu"

alias df="df -h" # human-readable sizes
alias free="free -h"     
alias sctl="sudo systemctl"


alias vim="nvim"
alias fm="vifm"
alias sfm="sudo vifm"
# list files installed by a package
alias pls="pacman -Ql" # followd by package name

# -------------------- DOTFILES -------------------- 
alias config="cd $HOME/.config/"
alias conf.vim="vim $HOME/.config/nvim/init.vim"
alias conf.prof="vim $HOME/.profile"
alias conf.zsh="vim $HOME/.zshrc"
alias source-prof="source $HOME/.profile"

# config command
# TODO: change dotfiles to cfg
alias config='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


# research & programming
alias phd="$HOME/Documents/phd"
alias code="$HOME/code"

# ------------------- FUNCTIONS--------------------

# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


crun(){
	make $1
	./$1 "${@:2}"
}


venv(){
    local virtualenv="${1:-general}"
    source "$HOME/.virtualenvs/$virtualenv/bin/activate"
}

# I'm lazy, so just commit with some machine info
function update-dots {
    config add -u && \
    config commit -m "Update $(date +"%Y-%m-%d %H:%M") $(uname -s)/$(uname -m)" && \
    config push
}

dropcaches(){
    local option="${1:-1}"
    echo $option | sudo tee /proc/sys/vm/drop_caches
}

update-all() {
    echo "---- Arch updates ----"
    yay -Syu
    echo "---- Flatpak updates ----"
    flatpak update
}

